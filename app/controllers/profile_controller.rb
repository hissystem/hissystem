class ProfileController < ApplicationController
  def index
    session[:user] = User.find_by(user_address: session[:login_uid])
    if Profile.find_by(user_id: session[:user]).nil?
      @profile = Profile.new
      render :new
    else
      @profile = Profile.find_by(user_id: session[:user])
    end
  end
  
  def new
    
  end
  
  def create
    user = User.find_by(user_address: session[:login_uid])
    @profile = Profile.new(message:params[:profile][:message], user_id: user.id)
    #user.profiles << @profile
    @profile.save
    session[:user] = User.find_by(user_address: session[:login_uid])
    render :index
  end
  
  def edit
    @profile = Profile.find_by(params[:id])
  end
  
  def update
    @profile = Profile.find_by(params[:id])
    @profile.update(message:params[:profile][:message])
    session[:user] = User.find_by(user_address: session[:login_uid])
    render :index
  end
end
