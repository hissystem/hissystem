class CreateWorkpartners < ActiveRecord::Migration
  def change
    create_table :workpartners do |t|
      t.string :workpartner_name
      t.string :workpartner_address
      t.string :user_id

      t.timestamps null: false
    end
  end
end
