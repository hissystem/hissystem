class User < ActiveRecord::Base
  validates :user_name, presence: true
  validates :user_address, presence: true
  
  has_many :events, dependent: :destroy
  belongs_to :profiles, dependent: :destroy
  has_many :workpartners   

  attr_accessor :password, :password_confirmation

  def password=(val)
    if val.present?
      self.pass = BCrypt::Password.create(val)
      puts "---"
    end
    @password = val
  end
  

  def self.authenticate(user_address, pass)
    user = User.find_by(user_address: user_address)
    #p(user.user_address)
    #p(user.pass)
    #p(BCrypt::Password.new(user.pass))
    if user
      if user.pass == BCrypt::Password.new(user.pass)
        user
      else
        nil
      end
    else
      nil
    end
  end
end

