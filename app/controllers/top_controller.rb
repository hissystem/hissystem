class TopController < ApplicationController
    def main
        p("main start")
        if session[:login_uid]
            @events = Event.all
            redirect_to top_index_path
        else
            render 'main'
        end
    end
    
    def login
        user = User.authenticate(params[:user_address], params[:pass])
        if user
            session[:login_uid] = params[:user_address]
            flash[:notice] = "ログインできました。"
            redirect_to root_path
        else
            flash[:notice] = "ログインできませんでした。"
            redirect_to root_path
        end
    end
    
    def logout
        session.delete(:login_uid)
        p(session[:login_uid])
        redirect_to root_path
    end  

    def index
        session[:user] = User.find_by(user_address: session[:login_uid])
        Event.find_by(user_id: session[:user])
        #event_asc = Event.order(:event_day).all
        @event_asc = []
        Event.order(:event_day).all.each do |f|
          if f.user_id == session[:user][:id]
              @event_asc << f
          end
        end
        
        @workpartner = Workpartner.all
        @now = Time.current
        
        id=User.find_by(user_address: session[:login_uid]).id
        @workpartners = Workpartner.where(user_id: id)
    end
end
