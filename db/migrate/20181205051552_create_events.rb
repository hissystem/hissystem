class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.date :event_day
      t.string :event_title
      t.string :event_message

      t.timestamps null: false
    end
  end
end
