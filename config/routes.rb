Rails.application.routes.draw do

  get 'top/logout'
  post 'top/login'
  get 'top/index'
  
  get 'user/change'
  post 'user/update'

  get 'event/list'
  get 'event/all'
  
  post 'workpartner/tagset'
  get 'workpartner/tagset/:id',to: 'workpartner#tagset'
  get 'workpartner/list'
  get 'workpartner/new'
  get 'workpartner/delete'
  get 'workpartner/tagdelete'
  #delete 'workpartner/tagdelete/:id',to: 'workpartner#tagdelete'
  post 'workpartner/create'
  post 'workpartner/tagcreate'
  #post 'workpartner/tagset/:id',to: 'workpartner#tagset'
  
  root 'top#main'

  resources :user
  resources :event
  resources :profile
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
