class Event < ActiveRecord::Base
  belongs_to :users
  
  validates :event_day, presence: true
  validates :event_title, presence: true
  validates :event_message, presence: true
  validates :event_time, presence: true
  validates :eventpartner_address, presence: true
end
