class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.string :workpartner_tag
      t.integer :workpartner_id

      t.timestamps null: false
    end
  end
end
