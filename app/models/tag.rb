class Tag < ActiveRecord::Base
    validates :workpartner_tag, presence: true
    
    has_many :workpartners
end
