class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :user_name
      t.string :user_address
      t.string :pass
      t.integer :workpartner_id

      t.timestamps null: false
    end
  end
end
