class WorkpartnerController < ApplicationController
  
  def tagset
    @tag=Tag.new
    p("@tag=", @tag)
    #p("workpartner.id=", workpartner.id)
    #@tags=Tag.where(workpartner_id: params[:workpartner_id])
    @workpartner=Workpartner.find(params[:id])
    p("@workpartner=", @workpartner)
    #tags = Tag.all
    @tags = []
    Tag.all.each do |f|
      if f.workpartner_id == @workpartner.id
        @tags << f
      end
    end
    p("@tags=", @tags)
    
  end
  
  def tagcreate
    @tag = Tag.new(workpartner_tag: params[:tag][:workpartner_tag], workpartner_id: params[:tag][:workpartner_id])
    if @tag.save
      tagsingle=Tag.find_by(workpartner_tag: params[:tag][:workpartner_tag], workpartner_id: params[:tag][:workpartner_id])
      wp=Workpartner.find(params[:tag][:workpartner_id])
      wpall=Workpartner.where(workpartner_address: wp.workpartner_address)
      if wpall.count > 1##ここ
        wpall.each do |e|##
          unless e.id == tagsingle.workpartner_id
            Tag.new(workpartner_tag: params[:tag][:workpartner_tag],workpartner_id: e.id).save
          end
        end
      end
      #redirect_to workpartner_tagset_path(partner_id: params[:tag][:partner_id])
      #redirect_to workpartner_tagset_path
      p("params[:tag][:workpartner_id]",params[:tag][:workpartner_id])
      redirect_to "/workpartner/tagset/#{params[:tag][:workpartner_id]}"
    else
      @tags=Tag.where(workpartner_id: params[:tag][:workpartner_id])
      @workpartner=Workpartner.find(params[:tag][:workpartner_id])
      render 'tagset'
    end
  end
  
  def tagdelete
    tag=Tag.find(params[:id])
    wp=Workpartner.find(tag.workpartner_id)
    wpall=Workpartner.where(workpartner_address: wp.workpartner_address)
    if wpall.count >1
      wpall.each do |e|
        unless e.id == wp.id
          Tag.find_by(workpartner_tag: tag.workpartner_tag,workpartner_id: e.id).destroy
        end
      end
    end
    Tag.find(params[:id]).destroy
    #redirect_to workpartner_tagset_path(partner_id: params[:workpartner_id])
    redirect_to "/workpartner/tagset/#{wp.id}"
  end
  
  def list
    session[:user] = User.find_by(user_address: session[:login_uid])
    p("session[:user]=", session[:user])
    @tags = Tag.all
    p("@tags=", @tags)
    id=User.find_by(user_address: session[:login_uid]).id
    p("id=", id)
    @workpartners = Workpartner.where(user_id: id)
    p("@workpartners=", @workpartners)
  end
  
  def new
    @workpartner=Workpartner.new
  end
  
  def create
    user = User.find_by(user_address: session[:login_uid])
    @workpartner = Workpartner.new(workpartner_name: params[:workpartner][:workpartner_name], 
                                   workpartner_address: params[:workpartner][:workpartner_address], 
                                   user_id: user.id)
    if @workpartner.save
      redirect_to top_index_path
    #id=User.find_by(user_address: session[:login_uid]).id
    #p("id=", id)
    #@workpartner = Workpartner.new(workpartner_name: params[:workpartner][:workpartner_name],
    #                               workpartner_address: params[:workpartner][:workpartner_address],
    #                               user_id: id)
    #if @workpartner.save
    #  if Workpartner.where(workpartner_address: params[:workpartner][:workpartner_address]).count > 1
    #    wptag_id = Workpartner.find_by(workpartner_address: params[:workpartner][:workpartner_address]).id
    #    wpid = Workpartner.find_by(workpartner_address: params[:workpartner][:workpartner_address],user_id: id).id
    #    tags=Tag.where(workpartner_id: wptag_id)
    #    tags.each do |e|
    #      Tag.new(workpartner_tag: e.workpartner_tag,workpartner_id: wpid).save
    #    end
    #  end
    #  redirect_to workpartner_list_path
      
    else
      render 'new'
    end
  end
  
  def delete
    Tag.where(workpartner_id: params[:id]).delete_all
    Workpartner.find(params[:id]).destroy
    redirect_to workpartner_list_path
  end
  
end
