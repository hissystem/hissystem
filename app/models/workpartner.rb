class Workpartner < ActiveRecord::Base
      validates :workpartner_name, presence: true
      validates :workpartner_address, presence: true
      
      #belongs_to :profiles
      has_many :users
      has_many :tags
end
