class EventController < ApplicationController
  
  def new
    @workpartners = Workpartner.all
    #user = User.find_by(user_address: session[:login_uid])
    #if @workpartners.user_id == user.id
    #  flash[:eventpartner] = "相手をまだ登録していません。まずは登録してください。"
    #  redirect_to top_index_path
    #else
    #end
    @event = Event.new
    #p(Workpartner.all)
  end
  
  def create
    user = User.find_by(user_address: session[:login_uid])
    @event = Event.new(event_title:params[:event][:event_title], event_day:params[:event][:event_day], event_message:params[:event][:event_message], event_time:params[:event][:event_time], eventpartner_address:params[:event][:eventpartner_address])
    if  Workpartner.find_by(workpartner_address: params[:event][:eventpartner_address]).nil?
      flash[:eventpartner] = "相手をまだ登録されていないかもしれません。まずは登録しましょう。" #Workpartner一覧表示ページ→メインページの時出たので、直す
      render :new
    else
      user.events << @event
      if @event.save
        redirect_to root_path
      else
        flash[:no] = "空白箇所があります。埋めてください。"
      render action: :new
      end
    end
  end
  
  def edit
    #p("params=", params[:id])
    @event = Event.find(params[:id])
    #p("edit @event =", @event)
  end
  
  def update
    event = Event.find_by(params[:id])
    event.update(event_title:params[:event][:event_title], event_day:params[:event][:event_day], event_message:params[:event][:event_message], event_time:params[:event][:event_time], eventpartner_address:params[:event][:eventpartner_address])
    redirect_to top_index_path
  end
  
  def destroy
    event = Event.find(params[:id])
    event.destroy
    redirect_to top_index_path
  end
  
  def all
    session[:user] = User.find_by(user_address: session[:login_uid])
    @event = Event.order(:event_day).all
    @workpartner = Workpartner.all
    @now = Time.current
  end

  def list
    session[:user] = User.find_by(user_address: session[:login_uid])
    @event = Event.order(:event_day).all
    @workpartner = Workpartner.all
    @now = Time.current
  end
  
  
=begin
  def con
    @wp = Workpartner.find_by(workpartner_address: params[:workpartner_address])
    unless @wp
        @err="そのアドレスは存在しません"
        render 'add'
    else
      render 'top/index'
    end
  end
=end
  
end
